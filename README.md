# Instructions

## Creating Runner VMs

1. [Install Terraform CLI](https://learn.hashicorp.com/terraform/getting-started/install.html)
2. [Install gcloud CLI](https://cloud.google.com/sdk/install#installation_options)
3. Log into `gcloud` - `gcloud auth application-default login`
4. Run `./setup.sh up -u https://gitlab.com -r YOUR_REGISTRATION_TOKEN`

When new VMs come up the runners will show up in GitLab:

![image](resources/registered.jpg)

## Tearing down the VMs

1. Run `./setup.sh down`.

When the VMs are deleted or downscaled the registered runners will automatically deregister.

# Creating base image

The base image has the GitLab Runner installed on it. From it VMs are then created.

Before starting set your default zone and project with gcloud, e.g:

```shell
$ gcloud config set compute/zone europe-west1-c
$ gcloud config set project group-verify-df9383
```

Then run the `create_image` script:

```shell
$ cd base
$ RUNNER_VERSION=12-9 ./create_image.sh
```

**Note**: To delete an existing image run:

```shell
$ gcloud compute images delete runner-base-img-$RUNNER_VERSION
```