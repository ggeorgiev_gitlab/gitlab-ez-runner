#!/bin/bash

if [ -z $RUNNER_VERSION ]; then
    echo "Specify RUNNER_VERSION!"
    exit 1
fi

echo "Creating VM with terraform..."
terraform apply \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$(gcloud config get-value project)" \
    -var="zone=$(gcloud config get-value compute/zone)"

echo "Initializing gitlab-runner..."
gcloud compute scp ./init_script.sh runner-base-vm-$RUNNER_VERSION:/tmp/init_script.sh
gcloud compute ssh runner-base-vm-$RUNNER_VERSION --command "bash /tmp/init_script.sh"

RUNNER_INSTALL_LOCATION=$(gcloud compute ssh runner-base-vm-$RUNNER_VERSION --command "which gitlab-runner")

if [ $RUNNER_INSTALL_LOCATION != "/usr/bin/gitlab-runner" ]; then
    echo "Incorrect runner install location $RUNNER_INSTALL_LOCATION"
    exit 1
fi

echo "Stopping VM..."
gcloud compute instances stop runner-base-vm-$RUNNER_VERSION

echo "Creating Base Image..."
gcloud compute images delete runner-base-img-$RUNNER_VERSION
gcloud compute images create runner-base-img-$RUNNER_VERSION \
  --source-disk runner-base-vm-$RUNNER_VERSION \
  --source-disk-zone $(gcloud config get-value compute/zone) \
  --family debian

gcloud compute images add-iam-policy-binding runner-base-img-$RUNNER_VERSION \
    --member='allAuthenticatedUsers' \
    --role='roles/compute.imageUser'


echo "Tearing down VM..."
terraform destroy \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$(gcloud config get-value project)" \
    -var="zone=$(gcloud config get-value compute/zone)"

