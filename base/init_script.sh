#!/bin/bash

sudo apt --fix-broken install -y
sudo apt install git -y

cd /tmp
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb
sudo rm -rf /home/gitlab-runner/.bash_logout
rm gitlab-runner_amd64.deb