variable "runner_version" {
    type = string
}

variable "project" {
    type = string
}

variable "zone" {
    type = string
}

provider "google" {
  project = var.project
  zone = var.zone
}

resource "google_compute_instance" "runner_base_vm_instance" {
  name         = "runner-base-vm-${var.runner_version}"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = google_compute_network.runner_base_default_network.self_link
    access_config {
    }
  }
}

resource "google_compute_firewall" "runner_base_default_firewall" {
  name    = "runner-base-default-firewall"
  network = google_compute_network.runner_base_default_network.self_link

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}

resource "google_compute_network" "runner_base_default_network" {
  name                    = "runner-base-default-network"
  auto_create_subnetworks = "true"
}
