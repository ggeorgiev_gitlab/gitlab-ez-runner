while getopts ":p:z:v:" opts; do
   case ${opts} in
      p) PROJECT=${OPTARG} ;;
      z) ZONE=${OPTARG} ;;
      v) RUNNER_VERSION=${OPTARG} ;;
   esac
done

if [ -z $PROJECT ]; then
    PROJECT=$(gcloud config get-value project)
    read -p "Using default project set from gcloud config '$PROJECT'. If you want to change the default zone you can use 'gcloud config set project my_project' or supply the -p parameter to this script (y/n)? " OK

    if [ $OK != "y" ]; then
        exit 1
    fi
fi

if [ -z $ZONE ]; then
    ZONE=$(gcloud config get-value compute/zone)
    read -p "Using default zone set from gcloud config '$ZONE'. If you want to change the default zone you can use 'gcloud config set compute/zone europe-west1-c' or supply the -z parameter to this script (y/n)? " OK

    if [ $OK != "y" ]; then
        exit 1
    fi
fi

if [ -z $RUNNER_VERSION ]; then
    RUNNER_VERSION=12-9
fi

terraform init > /dev/null
terraform destroy \
    -var='gitlab_url=unused' \
    -var='registration_token=unused' \
    -var='runner_name=unused' \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$PROJECT" \
    -var="zone=$ZONE"