variable "gitlab_url" {
  type = string
}

variable "registration_token" {
  type = string
}

variable "runner_name" {
  type = string
}

variable "runner_version" {
  type = string
  default = "12-10"
}

variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "image" {
  type = object({
    project = string
    name = string
  })

  default = {
    project = "group-verify-df9383" 
    name = "runner-base-img"
  }
}

variable "machine_type" {
  type = string
  default = "f1-micro"
}

provider "google" {
  project = var.project
  zone = var.zone
}

resource "google_compute_instance_template" "runner_template" {
  name        = "gitlab-runner-template"
  description = "This template is used to create gitlab-runner instances."

  machine_type         = var.machine_type

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    source_image = data.google_compute_image.runner_image.self_link
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = google_compute_network.runner_network.self_link
    access_config {
    }
  }

  metadata = {
      startup-script = <<SCRIPT
    
        gitlab-runner unregister --all-runners

        gitlab-runner register \
            --non-interactive \
            --url ${var.gitlab_url} \
            --registration-token ${var.registration_token} \
            --name `hostname`-${var.runner_name} \
            --executor shell

        systemctl enable gitlab-runner
        systemctl start gitlab-runner

      SCRIPT

      shutdown-script = "gitlab-runner unregister --all-runners"
  }
}

data "google_compute_image" "runner_image" {
  project = var.image.project
  name  = "${var.image.name}-${var.runner_version}"
}

resource "google_compute_target_pool" "runner_pool" {
  name = "runner-pool"
}

resource "google_compute_instance_group_manager" "runner_instance_manager" {
  name = "runner-instance-manager"

  version {
    instance_template  = google_compute_instance_template.runner_template.id
    name               = "primary"
  }

  target_pools       = [google_compute_target_pool.runner_pool.id]
  base_instance_name = "runner"
}

resource "google_compute_autoscaler" "runner_autoscaler" {
  name   = "runner-autoscaler"
  target = google_compute_instance_group_manager.runner_instance_manager.id

  autoscaling_policy {
    max_replicas    = 5
    min_replicas    = 1
    cooldown_period = 60

    cpu_utilization {
      target = 0.5
    }
  }
}

resource "google_compute_network" "runner_network" {
  name                    = "runner-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "runner_firewall" {
  name    = "runner-firewall"
  network = google_compute_network.runner_network.self_link

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}