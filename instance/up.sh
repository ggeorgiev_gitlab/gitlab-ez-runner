while getopts ":u:r:n:p:z:v:" opts; do
   case ${opts} in
      u) GITLAB_URL=${OPTARG} ;;
      r) REGISTRATION_TOKEN=${OPTARG} ;;
      n) RUNNER_NAME=${OPTARG} ;;
      p) PROJECT=${OPTARG} ;;
      z) ZONE=${OPTARG} ;;
      v) RUNNER_VERSION=${OPTARG} ;;
   esac
done

if [ -z $PROJECT ]; then
    PROJECT=$(gcloud config get-value project)
    read -p "Using default project set from gcloud config '$PROJECT'. If you want to change the default zone you can use 'gcloud config set project my_project' or supply the -p parameter to this script (y/n)? " OK

    if [ $OK != "y" ]; then
        exit 1
    fi
fi

if [ -z $ZONE ]; then
    ZONE=$(gcloud config get-value compute/zone)
    read -p "Using default zone set from gcloud config '$ZONE'. If you want to change the default zone you can use 'gcloud config set compute/zone europe-west1-c' or supply the -z parameter to this script (y/n)? " OK

    if [ $OK != "y" ]; then
        exit 1
    fi
fi

if [ -z $RUNNER_VERSION ]; then
    RUNNER_VERSION=12-9
fi

if [ -z $RUNNER_NAME ]; then
    RUNNER_NAME=auto
fi

terraform init > /dev/null
terraform apply \
    -var="gitlab_url=$GITLAB_URL" \
    -var="registration_token=$REGISTRATION_TOKEN" \
    -var="runner_name=$RUNNER_NAME" \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$PROJECT" \
    -var="zone=$ZONE"