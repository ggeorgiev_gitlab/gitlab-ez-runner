Getting a custom image from another project

https://cloud.google.com/compute/docs/images#os-compute-support
https://cloud.google.com/compute/docs/images/managing-access-custom-images#share-images-publicly
```
gcloud compute images list --project freebsd-org-cloud-dev --no-standard-images
```