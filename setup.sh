#!/bin/bash

if [ "$1" == "up" ];
then
    cd instance
    bash up.sh "${@:2}"
elif [ "$1" == "down" ];
then
    cd instance
    bash down.sh
else
    echo "only 'up' and 'down' are valid arguments"
fi